
-> Play framework with Mongo

// Project Requirements

sbt_version = 1.6.2
jdk_version = openjdk version "1.8.0_312"
scala_version = 2.13.8
play_framework version = 2.8.14
MongoDB shell version = 4.0.28

//How to set_up java home and path:
> open terminal
> paste command "gedit .bashrc"

paste the following path in file

export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin

>save and exit
-----------------------------------------------------------
//Dependencies 

libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "4.6.0"
libraryDependencies += "org.reactivestreams" % "reactive-streams" % "1.0.3"
libraryDependencies += "org.scala-lang" % "scala-library" % "2.13.8"

--------------------------------------------------------------
//Project_Description


File: app > controller > HR.scala

->login()
Login function takes you to the login view

->validate_login()
Validates wheater the user is authenticated or not by calling the validation model.

->create_user()
Takes you to the html view where you can enter new user details. 

->add user()
Adds the new user if not exist else returns "user alreadty exists" by
taking the data to model page.

->add_employee()
takes you to the page where you can fill new employee details.

-> val_employee()
adds new employee to the database.

->show_emp()
shows the list of all employees by taking you to html view.

-> delete()
deletes the employee from list by calling delete model.

-> update_emp()
takes you to the paghtml view where you can update employee details

->update_employee()
updates the employee details to db and displays it.

-> more_details()
showws the more details of the employees.
--------------------------------------------------------------

File: App->model-> manage_users.scala

->validate_user()
it is the model, where user validation is done at backend.

->add_employee()
here the employee details are sent from html view to be stored in database.

-> show_employee()
sends data from backend to the view to display employee details.

->remove_employee()
removes the employee from database.

->update_employee()
update employee details. 

-------------------------------------------------------------------


File: App->views
this folder contain the views for login page, create user,sign up page and show employee.

---------------------------------------------------------------------






