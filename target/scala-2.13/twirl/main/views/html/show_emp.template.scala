
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object show_emp extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template5[collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(fname:collection.mutable.ListBuffer[String],lname:collection.mutable.ListBuffer[String],
age:collection.mutable.ListBuffer[String],
dept:collection.mutable.ListBuffer[String])(implicit flash:Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.1*/("""

"""),_display_(/*6.2*/main("Employee List")/*6.23*/{_display_(Seq[Any](format.raw/*6.24*/("""
"""),format.raw/*7.1*/("""<div style="padding: 1cm; background-color:mistyrose;height:30cm">
    <form style="padding:0.5cm; float:right" action=""""),_display_(/*8.55*/routes/*8.61*/.HR.login),format.raw/*8.70*/("""">
        <button class="btn btn-primary" type="submit">Log Out!</button>
    </form>
    <form style="padding:0.5cm; float:right" action=""""),_display_(/*11.55*/routes/*11.61*/.HR.more_details),format.raw/*11.77*/("""">
        <button class="btn btn-primary" type="submit">More Details</button>
    </form>
    <form style="padding:0.5cm; float:right" action=""""),_display_(/*14.55*/routes/*14.61*/.HR.add_employee),format.raw/*14.77*/("""">
        <button class="btn btn-primary" type="submit">Add Employee</button>
    </form>
    <span style="color:red">"""),_display_(/*17.30*/flash/*17.35*/.get("msg")),format.raw/*17.46*/("""</span>
    <span style="color:green">"""),_display_(/*18.32*/flash/*18.37*/.get("msg1")),format.raw/*18.49*/("""</span>
    <table class="table" style="border:solid 1px;border-color: white; background-color:lavender">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Age</th>
            <th scope="col">Department</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        """),_display_(/*31.10*/for(i<- 0 to (fname.length)-1) yield /*31.40*/{_display_(Seq[Any](format.raw/*31.41*/("""
        """),format.raw/*32.9*/("""<tr>
            <th style="border:solid 1px, border-color: white" scope="row">"""),_display_(/*33.76*/(i+1)),format.raw/*33.81*/("""</th>
            <td style="border:solid 1px;border-color: white">"""),_display_(/*34.63*/fname(i)),format.raw/*34.71*/("""</td>
            <td style="border:solid 1px;border-color: white">"""),_display_(/*35.63*/lname(i)),format.raw/*35.71*/("""</td>
            <td style="border:solid 1px;border-color: white">"""),_display_(/*36.63*/age(i)),format.raw/*36.69*/("""</td>
            <td style="border:solid 1px;border-color: white">"""),_display_(/*37.63*/dept(i)),format.raw/*37.70*/("""</td>
            <td style="border:solid 1px;border-color: white">
                <div style="border-">
                    <form method="post" action=""""),_display_(/*40.50*/routes/*40.56*/.HR.update_emp),format.raw/*40.70*/("""">
                        <button style="float:left" class="btn btn-primary" name="index" type="submit" value=""""),_display_(/*41.111*/i),format.raw/*41.112*/("""">Update</button>
                    </form>
                    <form method="post" action=""""),_display_(/*43.50*/routes/*43.56*/.HR.delete),format.raw/*43.66*/("""">
                        <button style="margin-left:10px" class="btn btn-primary" name="index" type="submit" value=""""),_display_(/*44.117*/i),format.raw/*44.118*/("""">Delete</button>
                    </form>
                </div>
            </td>
        </tr>
        """)))}),format.raw/*49.10*/("""
        """),format.raw/*50.9*/("""</tbody>
    </table>
</div>
""")))}))
      }
    }
  }

  def render(fname:collection.mutable.ListBuffer[String],lname:collection.mutable.ListBuffer[String],age:collection.mutable.ListBuffer[String],dept:collection.mutable.ListBuffer[String],flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(fname,lname,age,dept)(flash)

  def f:((collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String]) => (Flash) => play.twirl.api.HtmlFormat.Appendable) = (fname,lname,age,dept) => (flash) => apply(fname,lname,age,dept)(flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  SOURCE: app/views/show_emp.scala.html
                  HASH: 14d4f7b8146aecebe04c6dd6f589415016cf4275
                  MATRIX: 883->1|1175->200|1203->203|1232->224|1270->225|1297->226|1444->347|1458->353|1487->362|1655->503|1670->509|1707->525|1879->670|1894->676|1931->692|2078->812|2092->817|2124->828|2190->867|2204->872|2237->884|2712->1332|2758->1362|2797->1363|2833->1372|2940->1452|2966->1457|3061->1525|3090->1533|3185->1601|3214->1609|3309->1677|3336->1683|3431->1751|3459->1758|3641->1913|3656->1919|3691->1933|3832->2046|3855->2047|3977->2142|3992->2148|4023->2158|4170->2277|4193->2278|4334->2388|4370->2397
                  LINES: 21->1|28->4|30->6|30->6|30->6|31->7|32->8|32->8|32->8|35->11|35->11|35->11|38->14|38->14|38->14|41->17|41->17|41->17|42->18|42->18|42->18|55->31|55->31|55->31|56->32|57->33|57->33|58->34|58->34|59->35|59->35|60->36|60->36|61->37|61->37|64->40|64->40|64->40|65->41|65->41|67->43|67->43|67->43|68->44|68->44|73->49|74->50
                  -- GENERATED --
              */
          