
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object show_more extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template5[collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(fname:collection.mutable.ListBuffer[String],lname:collection.mutable.ListBuffer[String],
salary:collection.mutable.ListBuffer[String],
location:collection.mutable.ListBuffer[String])(implicit flash:Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.1*/("""

"""),_display_(/*6.2*/main("Employee List")/*6.23*/{_display_(Seq[Any](format.raw/*6.24*/("""
"""),format.raw/*7.1*/("""<div style="padding: 1cm; background-color:mistyrose;height:30cm">
    <form style="padding:0.5cm; float:right" action=""""),_display_(/*8.55*/routes/*8.61*/.HR.show_emp),format.raw/*8.73*/("""">
        <button class="btn btn-primary" type="submit">Back</button>
    </form>
<!--    <form style="padding:0.5cm; float:right" action=""""),_display_(/*11.59*/routes/*11.65*/.HR.add_employee),format.raw/*11.81*/("""">-->
<!--        <button class="btn btn-primary" type="submit">Add Employee</button>-->
<!--    </form>-->
<!--    <span style="color:red">"""),_display_(/*14.34*/flash/*14.39*/.get("msg")),format.raw/*14.50*/("""</span>-->
<!--    <span style="color:green">"""),_display_(/*15.36*/flash/*15.41*/.get("msg1")),format.raw/*15.53*/("""</span>-->
    <table class="table" style="border:solid 1px;border-color: white; background-color:lavender">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Salary</th>
            <th scope="col">Location</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        """),_display_(/*28.10*/for(i<- 0 to (fname.length)-1) yield /*28.40*/{_display_(Seq[Any](format.raw/*28.41*/("""
        """),format.raw/*29.9*/("""<tr>
            <th style="border:solid 1px, border-color: white" scope="row">"""),_display_(/*30.76*/(i+1)),format.raw/*30.81*/("""</th>
            <td style="border:solid 1px;border-color: white">"""),_display_(/*31.63*/fname(i)),format.raw/*31.71*/("""</td>
            <td style="border:solid 1px;border-color: white">"""),_display_(/*32.63*/lname(i)),format.raw/*32.71*/("""</td>
            <td style="border:solid 1px;border-color: white">"""),_display_(/*33.63*/salary(i)),format.raw/*33.72*/("""</td>
            <td style="border:solid 1px;border-color: white ">"""),_display_(/*34.64*/location(i)),format.raw/*34.75*/("""</td>
<!--            <td style="border:solid 1px;border-color: white">"""),_display_(/*35.67*/location(i)),format.raw/*35.78*/("""</td>-->
<!--            <td style="border:solid 1px;border-color: white">-->
<!--                <div style="border-">-->
<!--                    <form method="post" action=""""),_display_(/*38.54*/routes/*38.60*/.HR.update_emp),format.raw/*38.74*/("""">-->
<!--                        <button style="float:left" class="btn btn-primary" name="index" type="submit" value=""""),_display_(/*39.115*/i),format.raw/*39.116*/("""">Update</button>-->
<!--                    </form>-->
<!--                    <form method="post" action=""""),_display_(/*41.54*/routes/*41.60*/.HR.delete),format.raw/*41.70*/("""">-->
<!--                        <button style="margin-left:10px" class="btn btn-primary" name="index" type="submit" value=""""),_display_(/*42.121*/i),format.raw/*42.122*/("""">Delete</button>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </td>-->
        </tr>
        """)))}),format.raw/*47.10*/("""
        """),format.raw/*48.9*/("""</tbody>
    </table>
</div>

""")))}))
      }
    }
  }

  def render(fname:collection.mutable.ListBuffer[String],lname:collection.mutable.ListBuffer[String],salary:collection.mutable.ListBuffer[String],location:collection.mutable.ListBuffer[String],flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(fname,lname,salary,location)(flash)

  def f:((collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String]) => (Flash) => play.twirl.api.HtmlFormat.Appendable) = (fname,lname,salary,location) => (flash) => apply(fname,lname,salary,location)(flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  SOURCE: app/views/show_more.scala.html
                  HASH: afa672a5bef74df4547d69afdc3ad8c2851f8841
                  MATRIX: 884->1|1183->207|1211->210|1240->231|1278->232|1305->233|1452->354|1466->360|1498->372|1666->513|1681->519|1718->535|1886->676|1900->681|1932->692|2005->738|2019->743|2052->755|2531->1207|2577->1237|2616->1238|2652->1247|2759->1327|2785->1332|2880->1400|2909->1408|3004->1476|3033->1484|3128->1552|3158->1561|3254->1630|3286->1641|3385->1713|3417->1724|3620->1900|3635->1906|3670->1920|3818->2040|3841->2041|3977->2150|3992->2156|4023->2166|4177->2292|4200->2293|4365->2427|4401->2436
                  LINES: 21->1|28->4|30->6|30->6|30->6|31->7|32->8|32->8|32->8|35->11|35->11|35->11|38->14|38->14|38->14|39->15|39->15|39->15|52->28|52->28|52->28|53->29|54->30|54->30|55->31|55->31|56->32|56->32|57->33|57->33|58->34|58->34|59->35|59->35|62->38|62->38|62->38|63->39|63->39|65->41|65->41|65->41|66->42|66->42|71->47|72->48
                  -- GENERATED --
              */
          