package models
import org.mongodb.scala.bson.BsonDocument
import Helpers._
import collection.mutable.ListBuffer
//mongo imports
import org.mongodb.scala._
import org.mongodb.scala.model._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.model.UpdateOptions
import org.mongodb.scala.bson.BsonObjectId

object manage_users {

    val client: MongoClient = MongoClient()
    val mongoClient: MongoClient = MongoClient("mongodb://127.0.0.1:27017/?gssapiServiceName=mongodb")
    val database = client.getDatabase("local")
    val collection = database.getCollection("Users")
    var collection2 = database.getCollection("Users2")
    var join = database.getCollection("join")

    private var users:Map[String, String] = Map("khush" -> "1180")
    private var fname:ListBuffer[String] = ListBuffer()//("Abhi","Puneet")
    private var lname:ListBuffer[String] = ListBuffer()//("Sehgal","Nanda")
    private var age:ListBuffer[String] = ListBuffer()//("21","23")
    private var salary:ListBuffer[String] = ListBuffer()//("45000", "65000")
    private var dept:ListBuffer[String] = ListBuffer()//("Tech","developer")
    private var location:ListBuffer[String] = ListBuffer()//("Banglore","Mohali")


    def validate_user(username: String, password: String): Boolean = {
        users.get(username).exists( _ == password)
    }

    def create_user(username: String, password: String): Boolean = {
        if(users.get(username).exists(_ == password)) false

        else{
            users += (username -> password)
            true
        }
    }
    def add_employee(fname_in:String, lname_in:String, age_in:String, salary_in:String, dept_in:String, location_in:String )= {
        fname += fname_in;  lname += lname_in
        age += age_in;      salary += salary_in
        dept += dept_in;    location += location_in

        val document = BsonDocument("First_name" ->  s"$fname_in" ,
            "Last_name" ->  s"$lname_in",
            "Age" -> s"$age_in",
            "Department" -> s"$dept_in"
        )

        val document2 = BsonDocument("First_name" -> s"$fname_in",
        "Last_name" -> s"$lname_in", "Salary" -> s"$salary_in", "Location"-> s"$location_in")

        val document3 = BsonDocument(
            "First_name" ->  s"$fname_in" ,
            "Last_name" ->  s"$lname_in",
            "Age" -> s"$age_in",
            "Department" -> s"$dept_in",
            "MoreDetails" -> BsonDocument("Salary" -> s"$salary_in",
                "Location"-> s"$location_in"
            )
        )

        collection.insertOne(document).printResults()
        collection2.insertOne(document2).printResults()
        join.insertOne(document3).printResults()
    }

    def show_employee()= {
       // val s = Aggregates.lookup(from = "Users2", "First_name", "First_name", "Heyy" )

      collection.aggregate(List(Aggregates.lookup("Users2", "First_name", "First_name", "MoreDetails")))
      //  collection.insertOne(s)

        collection.find().printResults()
        List(fname,lname,age,salary,dept,location)
    }

    def remove_employee(index:Int) ={

        val d = fname(index)
        collection.deleteOne(equal("First_name",s"$d" )).printResults()
        collection2.deleteOne(equal("First_name",s"$d")).printResults()
        join.deleteOne(equal("First_name",s"$d")).printResults()


        fname.remove(index);lname.remove(index)
        age.remove(index);salary.remove(index)
        dept.remove(index);location.remove(index)
    }


    def update_employee(index:Int, age_in:String, salary_in: String, dept_in:String, location_in:String )={

        var f = fname(index)
        collection.updateOne(
            equal("First_name", s"$f"),
            combine(set("Age", s"$age_in"), set("Salary", s"$salary_in"), set("Department",s"$dept_in"),
                set("Location",s"$location_in")))
          .printResults()

        collection2.updateOne(
            equal("First_name", s"$f"),
            combine(set("Salary", s"$salary_in"), set("Location",s"$location_in"))

        ).printResults()
        join.updateOne(
            equal("First_name", s"$f"),
            combine(set("Age", s"$age_in"), set("Department",s"$dept_in"),
                set("MoreDetails.Salary", s"$salary_in" ),
                set("MoreDetails.Location", s"$location_in"))

        ).printResults()


        age(index) = age_in; salary(index) = salary_in
        dept(index) = dept_in; location(index) = location_in
    }


}
